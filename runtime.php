<?php
PerchSystem::register_feather('Mix');

class PerchFeather_Mix extends PerchFeather
{
	public function manifest($path = "", $manifest = false, $shouldHotReload = false)
	{
		$public_path = $_SERVER["DOCUMENT_ROOT"];
		$hotreload_path = 'http://localhost:8080';
		$manifestFile = '/dist/app-manifest.json';

		if ( ! $manifest ) {
			static $manifest;
		}
		if ( ! $shouldHotReload ) {
			static $shouldHotReload;
		}
		if ( ! $manifest ) {
			$manifestPath    = $public_path . $manifestFile;
			$shouldHotReload = file_exists( $public_path . '/hot' );

			if ( ! file_exists( $manifestPath ) ) {
				throw new Exception(
					'The Laravel Mix manifest file does not exist. ' .
					'Please run "npm run dev" and try again.'
				);
			}

			if ($shouldHotReload)
			{
				$manifestPath    = $hotreload_path . $manifestFile;
				$manifest = json_decode( file_get_contents( $manifestPath ), true );
			}
			else
			{
				$manifest = json_decode( file_get_contents( $manifestPath ), true );
			}
		}
		if ( ! substr( $path, 0, strlen( $path ) ) === '/' ) {
			$path = "/{$path}";
		}
		if ( ! array_key_exists( $path, $manifest ) ) {
			throw new Exception(
				"Unknown Laravel Mix file path: {$path}. Please check your requested " .
				"webpack.mix.js output path, and try again."
			);
		}

		return $shouldHotReload
			? "http://localhost:8080{$manifest[$path]}"
			: $manifest[ $path ];
	}

	public function get_css($opts, $index, $count)
	{
		return $this->_single_tag('link', array(
			'rel' => 'stylesheet',
			'href' => $this->manifest($opts['file']),
			'type' => 'text/css'
		));
	}

	public function get_javascript($opts, $index, $count)
	{
		return $this->_script_tag(array(
			'src' => $this->manifest($opts['file'])
		));
	}
}
