# laravel MIX for perch cms

## Installation

```
git clone https://gitlab.com/nerdcel/perch-laravel-mix.git
mv perch-laravel-mix ${path_to_perch_featers}
```

In your perch config feathers.php file add:
```
include(PERCH_PATH.'/addons/feathers/perch-laravel-mix/runtime.php');
```

In your perch template use:

Styles
```
perch_get_css(["file" => "/dist/app.css"]); //change path to whatever fits your mix settings
```

Styles
```
perch_get_javascript(["file" => "/dist/app.js"]); //change path to whatever fits your mix settings
```
